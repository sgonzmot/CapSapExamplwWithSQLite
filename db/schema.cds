using {cuid} from '@sap/cds/common';

namespace db;

entity Question {
    key id       : Integer;
        question : String;
        answers  : Composition of many Answer
                       on answers.answer = id;
}

entity Users {
    key name     : String;
        password : String;
}

entity Answer : cuid {
    answer   : String;
    question : Association to Question;
}
