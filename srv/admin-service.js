const cds = require('@sap/cds')
const saltedMd5 = require('salted-md5');

module.exports = async (srv) => {
  const db = await cds.connect.to ('db')
  const { Users } = db.entities ('db')

  srv.before ('CREATE','Users', each => {
      const saltedHash = saltedMd5('admin', '-casa');
      
      each.data.password = saltedHash;
  })

  
}
