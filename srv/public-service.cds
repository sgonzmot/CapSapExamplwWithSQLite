using {db} from '../db/schema';

@(path : '/public/api')
service Service {

    entity Question as projection on db.Question;
    entity Answer   as projection on db.Answer;

    @readonly
    entity Users    as projection on db.Users;

}
