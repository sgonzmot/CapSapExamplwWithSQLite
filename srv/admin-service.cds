using {db} from '../db/schema';

@(path : '/admin/api')
@requires : 'authenticated'
service Admin {

    entity Question as projection on db.Question;
    entity Answer   as projection on db.Answer;
    entity Users    as projection on db.Users;

}
